#include "Class.h"


#include <iostream>
#include <conio.h> //holds the _getch() function

using namespace std; /*standard*/

int main() {

	cout << "Hello World!";


	int i;

	cout << "Enter an integer between 1/5";
	cin >> i;


	//if between 1 and 5
	if ((1 <= i) && (i <= 5))
	{
		//cout << "true .\n";	//debug

		for (; i > 0; i--)
		{
			cout << i << "\"Those who dare to fail miserably can achieve greatly.\"\t-John F. Kennedy\n";
		}
	}
	//otherwise display and error, pause, and exit
	else {
		cout << "Error: invalid parameter entered. Exiting...\n";
		return 0;
	}	
	return 0;
}